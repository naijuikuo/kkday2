import requests
import pandas as pd

#取得總頁數
res = requests.get('https://www.kkday.com/zh-tw/theme/ajax_get_tw_islands?experiencetype=755,756&sort=omdesc&page=1')

act = res.json()['data']

totalCount = act['totalCount']

totalPage = act['totalPage'] + 1

result = []

#頁數迴圈
for page in range(1,totalPage):
    
    base_url = 'https://www.kkday.com/zh-tw/theme/ajax_get_tw_islands?experiencetype=755,756&sort=omdesc&page={}'
    
    scrape_url = base_url.format(page)
    
    response = requests.get(scrape_url)
    
    activities = response.json()['data']
    
    #行程迴圈
    for activity in activities['prods']:
        
        title = activity['name']

        location = activity['cities'][0]['name']

        fee = activity['currency'] + ' ' + activity['display_price']

        rating_count = activity['rating_count']

        rating_star = activity['rating_star']

        result.append(dict(行程標題 = title, 行程地點 = location, 行程費用 = fee, 評價數 = rating_count, 評價星等 = rating_star))
    
#驗證總筆數    
if totalCount == len(result):
    
    df = pd.DataFrame.from_dict(result)

    df.to_csv(r'kkday_island.txt', index = False, header = True)

else:
    
    print('筆數不一致')